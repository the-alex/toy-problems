"""
Given a picture of a DFA, implement a program that vaidates input strings
according to the transitions.
"""

from dataclasses import dataclass, field
from typing import Dict


# Define the node
@dataclass
class State:
    name: str

    # Important to keep this because, like function default arguments, the
    # mutable structure is shared between instances.
    transitions: Dict[str, 'State'] = field(default_factory=dict)

    def add_transition(self, symbol, next_state):
        self.transitions[symbol] = next_state

    def get_next_state(self, symbol):
        return self.transitions.get(symbol)


@dataclass
class DFA:
    start_state: State
    accept_states: set

    def validate_string(self, string):
        current_state = self.start_state

        for symbol in string:
            current_state = current_state.get_next_state(symbol)
            if not current_state:
                return False

        return current_state.name in self.accept_states


# This is decent; let's try to represent a DFA and validate an input

TEST = [
    ("ac", False),
    ("abc", True),
    ("abbbc", True),
    ("abbbb", False),
    ("aabbbc", True),
]

state_a = State('a')
state_b = State('b')
state_c = State('c')

# Should be anything ^ab.+c$
state_a.add_transition('a', state_a)
state_a.add_transition('b', state_b)

state_b.add_transition('a', state_a)
state_b.add_transition('b', state_b)
state_b.add_transition('c', state_c)

state_c.add_transition('a', state_a)
state_c.add_transition('b', state_a)
state_c.add_transition('c', state_a)

dfa = DFA(start_state=state_a, accept_states={'c'})

for input_string, expected in TEST:
    answer = dfa.validate_string(input_string)
    assert answer == expected, f"failed on {input_string}"
